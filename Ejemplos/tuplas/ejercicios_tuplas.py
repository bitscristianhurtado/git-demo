#Creacion de tuplas
def primerEjemplo():
	mi_tupla=("pepe", 1, True, 5.1)
	print(mi_tupla)

#Convertir una tupla en una lista
def segundoEjemplo():
	mi_tupla=("pepe", 1, True, 5.1)
	mi_lista=list(mi_tupla)
	print(mi_lista)

#Convertir una lista en una tupla
def tercerEjemplo():
	mi_lista=["andrea", 3, False, 3.2]
	mi_tupla=tuple(mi_lista)
	print(mi_tupla)

#Verificar si algun valor esta en la tupla devolviendo un booleano
def cuartoEjemplo():
	mi_tupla=("pepe", 1, True, 5.1)
	print(1 in mi_tupla)

#Verificar la cantidad de un valor en la tupla
def quintoEjemplo():
	mi_tupla=("pepe", 1, True, 5.1)
	print(mi_tupla.count(1))

#Verificar cuantos valores tiene la tupla
def sextoEjemplo():
	mi_tupla=("pepe", 1, True, 5.1)
	print(len(mi_tupla))

#empaquetado de tuplas
def septimoEjemplo():
	mi_tupla="pepe", 1, True, 5.1
	print(mi_tupla)

#desempaquetado de tuplas
def octavoEjemplo():	
	mi_tupla=("pepe", 14, 1, 2020)
	nombre, dia, mes, agno = mi_tupla
	print(nombre)
	print(dia)
	print(mes)
	print(agno)

print("1 Ejemplo")
primerEjemplo()
print("2 Ejemplo")
segundoEjemplo()
print("3 Ejemplo")
tercerEjemplo()
print("4 Ejemplo")
cuartoEjemplo()
print("5 Ejemplo")
quintoEjemplo()
print("6 Ejemplo")
sextoEjemplo()
print("7 Ejemplo")
septimoEjemplo()
print("8 Ejemplo")
octavoEjemplo()


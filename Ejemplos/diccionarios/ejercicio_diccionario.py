
#Crear y acceder a un diccionario por medio de sus claves(clave:valor)
def primerEjemplo():
	mi_diccionario={"Alemania":"Berlin", "Francia":"Paris", "Reino Unido":"Londres", "Colombia":"Bogotá"}
	print(mi_diccionario["Francia"])

#Insertar un nuevo elemento y modificarlo
def segundoEjemplo():
	mi_diccionario={"Alemania":"Berlin", "Francia":"Paris", "Reino Unido":"Londres", "Colombia":"Bogotá"}
	mi_diccionario["Italia"]="Lisboa"
	print(mi_diccionario)
	mi_diccionario["Italia"]="Roma"
	print(mi_diccionario)

#Eliminar un elemento
def tercerEjemplo():
	mi_diccionario={"Alemania":"Berlin", "Francia":"Paris", "Reino Unido":"Londres", "Colombia":"Bogotá"}
	del mi_diccionario["Francia"]
	print(mi_diccionario)

#Mesclar varios tipos de datos
def cuartoEjemplo():
	mi_diccionario={"Alemania":"Berlin", 1:"Cantantes", "Futbolistas":2}
	print(mi_diccionario)

#Asignar valores con una tupla
def quintoEjemplo():
	mi_tupla=["España", "Francia", "Alemania", "Colombia"]
	mi_diccionario={mi_tupla[0]:"Madrid", mi_tupla[1]:"Paris", mi_tupla[2]:"Berlin", mi_tupla[3]:"Bogotá"}
	print(mi_diccionario)

#Almacenar una tupla
def sextoEjemplo():
	anillos=[1991, 1992, 1993, 1996, 1997, 1998]
	mi_diccionario={"Nombre":"Michael", "Apellido": "Jordan", "Equipo":"Chicago","Numero":23, "Anillos":anillos}
	print(mi_diccionario)

#Alacenar un diccionario en otro diccionario
def septimoEjemplo():
	anillos=[1991, 1992, 1993, 1996, 1997, 1998]
	temporadas = {"Temporadas":anillos}
	mi_diccionario={"Nombre":"Michael", "Apellido": "Jordan", "Equipo":"Chicago","Numero":23, "Anillos":temporadas}
	print(mi_diccionario)

#Metodo keys() devuelve las claves de un diccionario
def octavoEjemplo():
	mi_diccionario={"Alemania":"Berlin", "Francia":"Paris", "Reino Unido":"Londres", "Colombia":"Bogotá"}
	print(mi_diccionario.keys())

#Metodo value() devuelve los valores de un  diccionario
def novenoEjemplo():
	mi_diccionario={"Alemania":"Berlin", "Francia":"Paris", "Reino Unido":"Londres", "Colombia":"Bogotá"}
	print(mi_diccionario.values())
#Metodo len() devuelve longitud del diccionario
def decimoEjemplo():
	mi_diccionario={"Alemania":"Berlin", "Francia":"Paris", "Reino Unido":"Londres", "Colombia":"Bogotá"}
	print(len(mi_diccionario))
